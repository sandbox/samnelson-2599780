<?php

/**
 * @file classes for TFA basic plugin
 */

/**
 * Class TfaMasquerade
 */
class TfaMasquerade extends TfaBasePlugin implements TfaLoginPluginInterface {
  public function __construct(array $context) {
    parent::__construct($context);
  }

  /**
   * Allow login if the current user is masquerading from another user.
   */
  public function loginAllowed() {
    // Try to load masqing uid from masquerade table.
    global $user;
    $uid = db_query("SELECT uid_from FROM {masquerade} WHERE sid = :sid AND uid_as = :uid_as", array(
      ':sid' => session_id(),
      ':uid_as' => $user->uid,
    ))->fetchField();

    if ($uid > 0) {
      return TRUE;
    }
    return FALSE;
  }
}

/**
 * Class TfaTrustedBrowserSetup
 */
class TfaMasqueradeSetup extends TfaTrustedBrowser implements TfaSetupPluginInterface {

  public function __construct(array $context) {
    parent::__construct($context);
  }

  public function getSetupForm(array $form, array &$form_state) { return array(); }
  public function submitSetupForm(array $form, array &$form_state) { }
  public function validateSetupForm(array $form, array &$form_state) { return TRUE; }
}
